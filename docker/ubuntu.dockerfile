FROM ubuntu:18.04

RUN apt-get update && apt-get install -y git software-properties-common && \
    add-apt-repository ppa:ubuntu-toolchain-r/test && \
    apt-get update && apt-get install -y cmake gcc-9 g++-9

RUN ln -s /usr/bin/g++-9 /usr/bin/g++

COPY . /app
RUN mkdir /app/docker-build
WORKDIR /app/docker-build

RUN cmake .. && make