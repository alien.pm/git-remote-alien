cmake_minimum_required(VERSION 3.0.0)

project(git-remote-alien)

set(CMAKE_CXX_STANDARD 17)

set(SOURCE_FILES
        GitHelpers.h
        GitHelpers.cpp
        GitTypes.cpp
        GitTypes.h
        LineByLineReader.cpp
        LineByLineReader.h
        PathFinder.cpp
        PathFinder.h
        GitInteractor.cpp
        GitInteractor.h
        Handlers/IGitCommandHandler.h
        Handlers/Fetch.cpp
        Handlers/Fetch.h
        Remote/IRemoteProvider.h
        Handlers/List.cpp
        Handlers/List.h
        Remote/RemoteContext.h
        ObjectMeta.cpp
        ObjectMeta.h
        GitObjectWriter.cpp
        GitObjectWriter.h
        Handlers/Push.h
        Handlers/Push.cpp
        Remote/IRemoteFileWriter.h
        Remote/Providers/LocalFilesystemRemoteProvider.cpp
        Remote/Providers/LocalFilesystemRemoteProvider.h
        Remote/Providers/LocalFilesystemFileWriter.h
        Remote/Providers/LocalFilesystemFileWriter.cpp
        Remote/Providers/BaseFilesystemRemoteProvider.h
        Remote/Providers/BaseFilesystemRemoteProvider.cpp
        Remote/RemoteProviderSelector.h
        Remote/RemoteProviderSelector.cpp
        ProcessHelpers.h
        ProcessHelpers.cpp
        Readers.h
        Remote/Providers/RcloneRemoteProvider.h
        Remote/Providers/RcloneRemoteProvider.cpp
        Remote/Providers/RcloneFileWriter.h
        Remote/Providers/RcloneFileWriter.cpp
        Remote/Providers/RcloneHelpers.h
        Remote/Providers/RcloneHelpers.cpp
        Encryption/EncryptionStrategySelector.h
        Encryption/IEncryptionStrategy.h
        Encryption/StubEncryptionStrategy.h
        Encryption/StubEncryptionStrategy.cpp
        Encryption/EncryptionStrategySelector.cpp
        Encryption/AesCbc256KeyEncryptionStrategy.h
        Encryption/AesCbc256KeyEncryptionStrategy.cpp
    )

add_executable(git-remote-alien main.cpp ${SOURCE_FILES})

include_directories(
        .
        ../third-party/tiny-process-library
        ../third-party/plog/include
        ../third-party/openssl/include
)

target_link_libraries(git-remote-alien tiny-process-library)