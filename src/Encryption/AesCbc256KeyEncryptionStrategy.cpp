#include <string>
#include "AesCbc256KeyEncryptionStrategy.h"
#include "Readers.h"

namespace GitRemote::Encryption
{
    class Aes256EncryptionStrategy : public IEncryptionStrategy
    {
    public:
        ChunkReader MakeEncryptor(ChunkReader encryptedReader) override
        {
            return [encryptedReader(std::move(encryptedReader))](std::string_view chunk)
            {
                std::string encryptedString(chunk.size(), 'x');

                for (size_t i = 0; i < chunk.size(); ++i)
                {
                    encryptedString[i] = chunk[i] ^ 42;
                }

                encryptedReader(encryptedString);
            };
        }

        ChunkReader MakeDecryptor(ChunkReader decryptedReader) override
        {
            return [decryptedReader(std::move(decryptedReader))](std::string_view chunk)
            {
                std::string encryptedString(chunk.size(), 'x');

                for (size_t i = 0; i < chunk.size(); ++i)
                {
                    encryptedString[i] = chunk[i] ^ 42;
                }

                decryptedReader(encryptedString);
            };
        }
    };
}

GitRemote::Encryption::IEncryptionStrategySPtr
GitRemote::Encryption::MakeAes256KeyEnryptionStrategy(std::string_view params)
{
    return std::make_shared<Aes256EncryptionStrategy>();
}
