#ifndef GIT_REMOTE_ALIEN_AESCBC256KEYENCRYPTIONSTRATEGY_H
#define GIT_REMOTE_ALIEN_AESCBC256KEYENCRYPTIONSTRATEGY_H

#include "IEncryptionStrategy.h"

namespace GitRemote::Encryption
{
    IEncryptionStrategySPtr MakeAes256KeyEnryptionStrategy(std::string_view params);
};


#endif //GIT_REMOTE_ALIEN_AESCBC256KEYENCRYPTIONSTRATEGY_H
