#include <iostream>

#include "EncryptionStrategySelector.h"
#include "StubEncryptionStrategy.h"

namespace GitRemote::Encryption
{
    // url e.g. "alien://#encryptionStrategyName:params^remoteProvider:..."
    IEncryptionStrategySPtr SelectEncryptionStrategy(std::string_view url)
    {
        size_t strategyNameBegin = url.find('#');
        if (strategyNameBegin == std::string_view::npos)
        {
            std::cerr << "Encryption strategy is not set. Encryption is disabled" << std::endl;
            return MakeStubEnryptionStrategy(url);
        }

        ++strategyNameBegin;

        size_t remoteProviderBegin = url.find('@');

        if (remoteProviderBegin == std::string_view::npos)
        {
            throw std::runtime_error("Remote provider is not set");
        }

        std::string_view strategyView = url.substr(
            strategyNameBegin,
            remoteProviderBegin - strategyNameBegin
        );

        size_t paramsBegin = strategyView.find(':');

        std::string_view strategyName;
        std::string_view strategyParams;

        if (paramsBegin == std::string_view::npos)
        {
            strategyName = strategyView;
        }
        else
        {
            strategyName = strategyView.substr(0, paramsBegin);
            strategyParams = strategyView.substr(paramsBegin + 1);
        }

        std::cerr  << "Using encryption strategy " << strategyName
                   << " with params " << strategyParams << std::endl;

        if (strategyName == "stub")
        {
            return MakeStubEnryptionStrategy(strategyParams);
        }

        if (strategyName == "aes256-key")
        {
            return MakeStubEnryptionStrategy(strategyParams);
        }

        throw std::runtime_error("Unknown encryption strategy " + std::string(strategyName));
    }
}
