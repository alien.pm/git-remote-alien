#ifndef GIT_REMOTE_ALIEN_ENCRYPTIONSTRATEGYSELECTOR_H
#define GIT_REMOTE_ALIEN_ENCRYPTIONSTRATEGYSELECTOR_H

#include "IEncryptionStrategy.h"

namespace GitRemote::Encryption
{
    IEncryptionStrategySPtr SelectEncryptionStrategy(std::string_view url);
}

#endif //GIT_REMOTE_ALIEN_ENCRYPTIONSTRATEGYSELECTOR_H
