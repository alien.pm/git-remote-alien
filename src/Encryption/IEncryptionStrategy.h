#ifndef GIT_REMOTE_ALIEN_IENCRYPTIONSTRATEGY_H
#define GIT_REMOTE_ALIEN_IENCRYPTIONSTRATEGY_H

#include <memory>
#include <functional>
#include "Readers.h"

namespace GitRemote::Encryption
{
    class IEncryptionStrategy
    {
    public:
        virtual ChunkReader MakeEncryptor(ChunkReader encryptedReader) = 0;
        virtual ChunkReader MakeDecryptor(ChunkReader decryptedReader) = 0;

        virtual ~IEncryptionStrategy() = default;
    };

    using IEncryptionStrategySPtr = std::shared_ptr<IEncryptionStrategy>;
    using EncryptionStrategyFactory = std::function<
            IEncryptionStrategySPtr(std::string_view arguments)
    >;
};

#endif //GIT_REMOTE_ALIEN_IENCRYPTIONSTRATEGY_H
