#include "StubEncryptionStrategy.h"
#include "Readers.h"

namespace GitRemote::Encryption
{
    class StubEncryptionStrategy : public IEncryptionStrategy
    {
    public:
        ChunkReader MakeEncryptor(ChunkReader encryptedReader) override
        {
            return [encryptedReader(std::move(encryptedReader))](std::string_view chunk)
            {
                encryptedReader(chunk);
            };
        }

        ChunkReader MakeDecryptor(ChunkReader decryptedReader) override
        {
            return [decryptedReader(std::move(decryptedReader))](std::string_view chunk)
            {
                decryptedReader(chunk);
            };
        }
    };

    IEncryptionStrategySPtr MakeStubEnryptionStrategy(std::string_view /* params */)
    {
        return std::make_shared<StubEncryptionStrategy>();
    }
}
