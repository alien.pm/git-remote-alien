#ifndef GIT_REMOTE_ALIEN_STUBENCRYPTIONSTRATEGY_H
#define GIT_REMOTE_ALIEN_STUBENCRYPTIONSTRATEGY_H

#include "IEncryptionStrategy.h"

namespace GitRemote::Encryption
{
    IEncryptionStrategySPtr MakeStubEnryptionStrategy(std::string_view params);
};

#endif //GIT_REMOTE_ALIEN_STUBENCRYPTIONSTRATEGY_H
