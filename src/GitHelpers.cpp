#include "GitHelpers.h"
#include "ProcessHelpers.h"
#include "GitTypes.h"
#include "LineByLineReader.h"
#include "GitHelpers.h"
#include "PathFinder.h"

#include <iterator>
#include <algorithm>
#include <process.hpp>
#include <sstream>

using namespace GitRemote;
using namespace GitRemote::ProcessHelpers;

namespace proc = TinyProcessLib;


UnexpectedGitOutput::UnexpectedGitOutput():
    std::runtime_error("Unexpected git output") {}


namespace GitRemote::GitHelpers
{
    CommandArguments GitStartupParams(
            const CommandArguments& arguments
    )
    {
        static std::optional<std::filesystem::path> gitPath =
        []()
        {
            auto gitPath = FindPATHFile("git");
            if (!gitPath)
            {
                gitPath = FindPATHFile("git.exe");
            }

            return gitPath;
        }();

        if (!gitPath)
        {
            throw std::runtime_error("Git binary not found in system PATH");
        }

        CommandArguments result;
        result.reserve(arguments.size() + 1);

        result.push_back(ProcessStringType(*gitPath));
        result.insert(result.end(), arguments.begin(), arguments.end());

        return result;
    }

    ProcessStringType RepositoryPath()
    {
        return {};
    }

    void CommandReader(
            const CommandArguments& arguments,
            ChunkReader outputReader)
    {
        ReadProcessOutput(
                GitStartupParams(arguments),
                std::move(outputReader),
                RepositoryPath()
        );
    }

    std::string CommandString(const CommandArguments& arguments)
    {
        std::ostringstream os;
        CommandReader(arguments, [&os](std::string_view chunk)
        {
            os.write(chunk.data(), chunk.size());
        });

        std::string result = os.str();
        if (!result.empty() && result[result.size() - 1] == '\n')
        {
            result.resize(result.size() - 1);
        }

        return result;
    }

    bool CommandOk(const CommandArguments& arguments)
    {
        return ProcessExitOk(GitStartupParams(arguments), RepositoryPath());
   }

    bool ObjectExists(const SHA1& sha)
    {
        return CommandOk({"cat-file", "-t", std::string(sha)});
    }

    bool HistoryExists(const SHA1& sha)
    {
        return CommandOk({"rev-list", "--objects", std::string(sha)});
    }

    SHA1 RefValue(std::string_view ref)
    {
        std::string raw = CommandString({"rev-parse", std::string(ref)});

        return SHA1::FromString(std::move(raw));
    }


    void SetSymbolicRef(std::string_view name, std::string_view value)
    {
        CommandString({"symbolic-ref", std::string(name), std::string(value)});
    }


    std::string SymbolicRefValue(std::string_view name)
    {
        return CommandString({"symbolic-ref", std::string(name)});
    }

    GitObjectType ObjectType(std::string_view object)
    {
        const std::string output = CommandString({"cat-file", "-t", std::string(object)});

        return ParseObjectType(output);
    }


    void ReadObject(std::string_view rev, std::optional<GitRemote::GitObjectType> type,
                                ChunkReader reader)
    {
        CommandArguments arguments = {"cat-file"};

        if (type)
        {
            arguments.push_back(ObjectTypeToString(*type));
        }
        else
        {
            arguments.push_back("-p");
        }

        arguments.push_back(rev);

        CommandReader(arguments, std::move(reader));
    };

    SHA1Vt RevList(const std::string &rev, const SHA1Vt &excludeList)
    {
        CommandArguments arguments{"rev-list", "--objects", rev};

        for (const SHA1& sha: excludeList)
        {
            if (ObjectExists(sha))
            {
                arguments.push_back("^" + std::string { sha });
            }

            // Todo log if missing
        }

        SHA1Vt objects;

        CommandReader(arguments, LineByLineReader(
                [&objects](std::string_view line)
                {
                    if (line.size() < 40)
                    {
                        throw std::runtime_error("Wrong rev-list output line: "
                                                 + std::string(line));
                    }

                    std::string shaValue{ line.data(), 40 };
                    objects.push_back(SHA1::FromString(std::move(shaValue)));
                }));

        return objects;
    }

    namespace
    {
        SHA1 ReadTagCommit(std::string_view rev)
        {
            std::optional<SHA1> tagCommit;

            ReadObject(rev, GitObjectType::Tag,
                                   LineByLineReader([&tagCommit](std::string_view line)
                                                    {
                                                        //Line sample:
                                                        //object fd21811a16d0f5963a52755674a16d0a347f1dfb

                                                        const size_t delPos = line.find(' ');
                                                        if (delPos == std::string::npos)
                                                        {
                                                            throw UnexpectedGitOutput(); // Todo: log
                                                        }

                                                        std::string ref {line.substr(delPos + 1)};
                                                        tagCommit = SHA1::FromString(std::move(ref));

                                                        throw LineByLineReader::DoneReadingException{};
                                                    }));

            if (!tagCommit)
            {
                throw UnexpectedGitOutput{};
            }

            return *tagCommit;
        }

        SHA1Vt ReadTreeObjects(std::string_view ref)
        {
            SHA1Vt objects;

            ReadObject(ref, {}, // Pretty-print output
                                   LineByLineReader([&objects](std::string_view line)
                                                    {
                                                        //Line sample:
                                                        //100644 blob 3b18e512dba79e4c8300dd08aeb37f8e728b8dad\thelloworld.txt

                                                        std::stringstream sLine;
                                                        sLine << line;

                                                        std::string mode;
                                                        std::string type;
                                                        std::string sha;

                                                        if (!std::getline(sLine, mode, ' ') ||
                                                            !std::getline(sLine, type, ' ') ||
                                                            !std::getline(sLine, sha,  '\t'))
                                                        {
                                                            throw UnexpectedGitOutput();
                                                        }

                                                        // Throws if sha is invalid
                                                        SHA1 sha1Val = SHA1::FromString(sha);

                                                        // Todo: cover with a test
                                                        bool isSubmodule =
                                                                mode == "160000"
                                                                && type == "commit";

                                                        if (isSubmodule)
                                                        {
                                                            return;
                                                        }

                                                        objects.push_back(std::move(sha1Val));
                                                    }));

            return objects;
        }

        SHA1Vt ReadCommitObjects(std::string_view ref)
        {
            SHA1Vt references;

            /* Output example
             *tree db4fe0bb2ba603280b444d32670944ee1278f24b
             *parent d7e6010009ab4bbf1a98834c2b5880ab60e996a3
             *author Igor ....
             *committer Igor ...
             */

            ReadObject(ref, {}, // Pretty-print output
                                   LineByLineReader([&references](std::string_view line)
                                                    {
                                                        std::stringstream sLine;
                                                        sLine << line;

                                                        std::string type;
                                                        if(!std::getline(sLine, type, ' '))
                                                        {
                                                            return;
                                                        }

                                                        if (type == "tree" ||
                                                            type == "parent")
                                                        {
                                                            std::string sha;

                                                            sLine >> sha;

                                                            // Throws on if SHA1 is invalid
                                                            SHA1 shaValue = SHA1::FromString(sha);
                                                            references.push_back(std::move(shaValue));
                                                        }
                                                    }));

            return references;
        }
    }

    SHA1Vt ReferencedObjects(std::string_view ref)
    {
        SHA1Vt references;

        const GitObjectType type = ObjectType(ref);

        switch (type)
        {
            case GitObjectType::Blob:
                break;
            case GitObjectType::Tag:
                references.push_back(ReadTagCommit(ref));
                break;
            case GitObjectType::Tree:
                references = ReadTreeObjects(ref);
                break;
            case GitObjectType::Commit:
                references = ReadCommitObjects(ref);
                break;
        }

        return references;
    }

    std::string RemoteUrl(std::string_view remoteName)
    {
        CommandArguments args{"remote", "get-url"};
        args.push_back(remoteName);

        return CommandString(args);
    }

    bool IsAncestorOf(const SHA1 &ancestor, const SHA1& ref)
    {
        return CommandOk({"merge-base", "--is-ancestor", ancestor, ref});
    }
}
