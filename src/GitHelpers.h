#ifndef SRC_GITHELPERS_H
#define SRC_GITHELPERS_H

#include "GitTypes.h"

#include <initializer_list>
#include <string>
#include <vector>
#include <optional>

#include "ProcessHelpers.h"

namespace GitRemote
{
    class UnexpectedGitOutput: public std::runtime_error
    {
    public:
        explicit UnexpectedGitOutput();
    };

    namespace GitHelpers
    {
        ProcessHelpers::CommandArguments GitStartupParams(const ProcessHelpers::CommandArguments& arguments);
        ProcessHelpers::ProcessStringType RepositoryPath();

        // Execute git command, read the output in callback.
        // Throws ErrorExitException
        void CommandReader(const ProcessHelpers::CommandArguments& arguments, ChunkReader reader);
        std::string CommandString(const ProcessHelpers::CommandArguments& arguments);
        bool CommandOk(const ProcessHelpers::CommandArguments& arguments);

        bool ObjectExists(const SHA1& sha);
        bool HistoryExists(const SHA1& sha); // Currently is not used
        SHA1 RefValue(std::string_view refPath);
        void SetSymbolicRef(std::string_view name, std::string_view value); // Used in test only
        std::string SymbolicRefValue(std::string_view name);
        GitObjectType ObjectType(std::string_view object);

        void ReadObject(
                std::string_view object,
                std::optional<GitRemote::GitObjectType> type,
                ChunkReader objectReader
        );

        SHA1Vt RevList(const std::string& rev, const SHA1Vt& excludeList = {});
        SHA1Vt ReferencedObjects(std::string_view rev);
        bool IsAncestorOf(const SHA1& ancestor, const SHA1& ref);
        std::string RemoteUrl(std::string_view remoteName); // Currently is not used
    };

} // ~namespace GitRemote

#endif //SRC_GITHELPERS_H
