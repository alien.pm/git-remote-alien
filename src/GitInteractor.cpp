#include "GitInteractor.h"
#include <iostream>
#include <sstream>

using namespace GitRemote;
using namespace GitRemote::Handlers;

GitInteractor::WrongCommandException::WrongCommandException(std::string command)
    : std::runtime_error("Wrong command " + std::move(command))
{}

GitInteractor::GitInteractor(
        std::istream& in,
        std::ostream& out,
        CommandHandlerMap commandHandlers)
        : in_(in), out_(out), handlers_(std::move(commandHandlers))
{}

void GitInteractor::CommunicationLoop()
{
    std::string line;

    while(std::getline(in_, line))
    {
        if (line.empty())
        {
            break;
        }

        std::string command;
        std::stringstream sLine { line };
        sLine >> command;

        if (command == "capabilities")
        {
            WriteCapabilities();
            continue;
        }

        auto commandHandler = handlers_.find(command);
        if (commandHandler == handlers_.end())
        {
            // Todo: log
            throw WrongCommandException{std::move(command)};
        }

        commandHandler->second->HandleGitCommand(std::move(line));
    };
}

void GitInteractor::WriteCapabilities()
{
    for (const auto& [command, handler]: handlers_)
    {
        out_ << command << std::endl;
    }

    out_ << std::endl;
}
