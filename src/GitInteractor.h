#ifndef GIT_REMOTE_ALIEN_GITINTERACTOR_H
#define GIT_REMOTE_ALIEN_GITINTERACTOR_H

#include <stdexcept>
#include <iostream>
#include <map>
#include <memory>

#include "Handlers/IGitCommandHandler.h"

namespace GitRemote
{
    using CommandHandlerMap = std::map<
        std::string, /* command */
        std::unique_ptr<Handlers::IGitCommandHandler>
    >;

    class GitInteractor
    {
    public:
        class WrongCommandException: std::runtime_error
        {
        public:
            explicit WrongCommandException(std::string command);
        };

        GitInteractor(std::istream& in, std::ostream& out,
                      CommandHandlerMap commandHandlers);

    public:
        void CommunicationLoop();

    private:
        void WriteCapabilities();

    private:
        std::istream& in_;
        std::ostream& out_;
        CommandHandlerMap handlers_;

    };
}

#endif //GIT_REMOTE_ALIEN_GITINTERACTOR_H
