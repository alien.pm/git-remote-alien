#include <process.hpp>
#include <sstream>

#include "GitHelpers.h"
#include "GitObjectWriter.h"
#include "ProcessHelpers.h"

namespace proc = TinyProcessLib;
using namespace GitRemote::ProcessHelpers;

// Todo: cover all cases

namespace GitRemote::GitHelpers
{
    struct GitObjectWriter::Impl
    {
        std::ostringstream stdoutAccumulator;
        std::ostringstream stderrAccumulator;
        std::optional<proc::Process> process;
    };

    GitObjectWriter WriteObject(GitObjectType type)
    {
        auto impl = std::make_unique<GitObjectWriter::Impl>();

        impl->process.emplace(
            GitStartupParams({
                "hash-object",
                "-w",
                "--stdin",
                "-t",
                ObjectTypeToString(type)
            }),
            RepositoryPath(),
            [implPtr(impl.get())](const char* data, size_t size) // Stdout handler
            {
                implPtr->stdoutAccumulator.write(data, size);
            },
            [implPtr(impl.get())](const char* data, size_t size){
                implPtr->stderrAccumulator.write(data, size);
            }, // Stderr handler // Todo: implement logging logic
            true // Open stdin
        );

        return GitObjectWriter(std::move(impl));
    }

    bool GitObjectWriter::IsFinalized() const
    {
        return !impl_ || !impl_->process.has_value();
    }

    void GitObjectWriter::EnsureNotFinalized()
    {
        if (IsFinalized())
        {
            throw std::runtime_error("Writer is already finalized");
        }
    }

    void GitObjectWriter::Write(std::string_view chunk)
    {
        EnsureNotFinalized();

        impl_->process->write(chunk.data(), chunk.size());
    }

    SHA1 GitObjectWriter::Finalize()
    {
        EnsureNotFinalized();

        impl_->process->close_stdin();

        const int ec = impl_->process->get_exit_status();

        const std::string outputHash = impl_->stdoutAccumulator.str();
        const std::string stdErr = impl_->stderrAccumulator.str();

        if (ec != 0)
        {
            throw ProcessHelpers::ErrorExitException(ec);
        }

        impl_->process.reset();

        return SHA1::FromString(outputHash);
    }

    GitObjectWriter::GitObjectWriter(std::unique_ptr<Impl> impl)
        : impl_(std::move(impl))
    { }

    GitObjectWriter::~GitObjectWriter()
    {
        if (!IsFinalized())
        {
            std::cerr << "GitObjectWriter destroyed before finalization" << std::endl;
            std::terminate();
        }
    }

    GitObjectWriter::GitObjectWriter(GitObjectWriter &&oth) noexcept
    {
        std::swap(impl_, oth.impl_);
    }

    GitObjectWriter& GitObjectWriter::operator=(GitObjectWriter &&oth) noexcept
    {
        std::swap(impl_, oth.impl_);

        return *this;
    }
}

