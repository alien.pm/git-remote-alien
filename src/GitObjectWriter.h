#ifndef GIT_REMOTE_ALIEN_GITOBJECTWRITER_H
#define GIT_REMOTE_ALIEN_GITOBJECTWRITER_H

#include "GitTypes.h"

namespace GitRemote::GitHelpers
{
    class GitObjectWriter
    {
    public:
        GitObjectWriter(GitObjectWriter&& oth) noexcept;
        GitObjectWriter& operator=(GitObjectWriter&& oth) noexcept;

        void Write(std::string_view chunk);
        SHA1 Finalize();
        bool IsFinalized() const;

        // Calls std::terminate if not finalized
        ~GitObjectWriter();

    private:
        struct Impl;

        explicit GitObjectWriter(std::unique_ptr<Impl> impl);
        void EnsureNotFinalized();
    private:
        friend GitObjectWriter WriteObject(GitObjectType type);

        std::unique_ptr<Impl> impl_;
    };

    GitObjectWriter WriteObject(GitObjectType type);
}


#endif //GIT_REMOTE_ALIEN_GITOBJECTWRITER_H
