#include <array>
#include "GitTypes.h"

using namespace std::string_literals;
using namespace GitRemote;

GitRemote::WrongObjectTypeException::WrongObjectTypeException(std::string_view type)
        : std::runtime_error("Wrong object type" + std::string(type)) {}

GitRemote::WrongSHA1StringException::WrongSHA1StringException(std::string_view value)
    : std::runtime_error("Wrong SHA1 string: " + std::string(value)) {}

SHA1::SHA1(std::string value) : value_(std::move(value))
{}

SHA1::operator const std::string&() const
{
   return value_;
}

SHA1::operator std::string_view() const
{
    return value_;
}

bool SHA1::operator==(const SHA1 &oth) const
{
    return value_ == oth.value_;
}


namespace
{
    using ObjectTypeString = std::pair<GitObjectType, std::string>;

    const std::array<ObjectTypeString, 4> OBJECT_TYPES
    {
           ObjectTypeString {GitObjectType::Commit, "commit"},
           ObjectTypeString {GitObjectType::Blob, "blob"},
           ObjectTypeString {GitObjectType::Tree, "tree"},
           ObjectTypeString {GitObjectType::Tag, "tag"}
    };
}

GitRemote::GitObjectType GitRemote::ParseObjectType(std::string_view type)
{
    auto it = find_if(OBJECT_TYPES.begin(), OBJECT_TYPES.end(),
    [&type](const ObjectTypeString& ots)
    {
       return ots.second == type;
    });

    if (it == OBJECT_TYPES.end())
    {
        throw WrongObjectTypeException(type);
    }

    return it->first;
}

std::string GitRemote::ObjectTypeToString(GitRemote::GitObjectType objectType)
{
    return find_if(OBJECT_TYPES.begin(), OBJECT_TYPES.end(), [&](const ObjectTypeString& ots){
        return ots.first == objectType;
    })->second;
}

std::ostream& GitRemote::operator<<(std::ostream &s, const Ref &ref)
{
    s << static_cast<const std::string&>(ref.sha)
      << " " << ref.path;

    return s;
}

std::string GitRemote::ParseRefFile(std::string_view content)
{
    const std::string prefixValue = "ref: ";

    size_t pos = content.find(prefixValue);

    if (pos == std::string::npos)
    {
        throw std::runtime_error("Wrong ref file content " +
            std::string(content));
    }

    return std::string(content.substr(prefixValue.size()));
}

SHA1 SHA1::FromString(std::string value)
{
    if (value.size() == 41 && value.back() == '\n')
    {
        //assert(false && "SHA1 string contains extra newline character");

        value.resize(40);
    }

    if (value.size() == 40)
    {
        return SHA1 {std::move(value)};
    };


    throw WrongSHA1StringException(std::move(value));
}

