#ifndef GIT_REMOTE_ALIEN_GITTYPES_H
#define GIT_REMOTE_ALIEN_GITTYPES_H

#include <string>
#include <vector>
#include <stdexcept>
#include <functional>
#include <iostream>

namespace GitRemote
{
    enum class GitObjectType: char
    {
        Blob   = 'b',
        Tree   = 't',
        Commit = 'c',
        Tag    = 'g'
    };

    class WrongSHA1StringException: public std::runtime_error
    {
    public:
        explicit WrongSHA1StringException(std::string_view value);
    };

    class SHA1
    {
    public:
        // Throws WrongSHA1StringException
        static SHA1 FromString(std::string value);
        operator const std::string&() const;
        operator std::string_view() const;
        bool operator==(const SHA1& oth) const;

    private:
        explicit SHA1(std::string value);

    private:
        std::string value_;
    };

    using SHA1Vt = std::vector<SHA1>;

    std::string ObjectTypeToString(GitObjectType objectType);

    class WrongObjectTypeException: public std::runtime_error
    {
    public:
        explicit WrongObjectTypeException(std::string_view type);
    };

    GitObjectType ParseObjectType(std::string_view type);

    struct Ref
    {
        std::string path;
        SHA1 sha;
    };

    // "ref: refs/heads/master" => "refs/heads/master"
    std::string ParseRefFile(std::string_view content);

    std::ostream& operator<<(std::ostream& s, const Ref& ref);

    using RefVt = std::vector<Ref>;
};

#endif //GIT_REMOTE_ALIEN_GITTYPES_H
