#include <sstream>
#include <queue>
#include <unordered_set>

#include "Fetch.h"
#include "GitHelpers.h"
#include "ObjectMeta.h"
#include "GitObjectWriter.h"

using namespace GitRemote;
using namespace GitRemote::Handlers;

Fetch::Fetch(
        std::istream &in,
        std::ostream &out,
        Remote::IRemoteProviderSPtr remote,
        Encryption::IEncryptionStrategySPtr encryptionStrategy
)
    : in_(in), out_(out), remote_(std::move(remote)), encryptionStrategy_(std::move(encryptionStrategy))
{ }

void Fetch::HandleGitCommand(std::string_view commandLine)
{
    std::string line { commandLine };

    do
    {
        if (line.empty())
        {
            break;
        }

        std::string stub;
        std::string sha;
        std::string value;

        std::stringstream sLine (line);
        if (!std::getline(sLine, stub, ' ') ||
            !std::getline(sLine, sha,  ' '))
        {
            // Todo: log
            throw UnexpectedGitOutput();
        }

        SHA1 objectSha = SHA1::FromString(sha);
        FetchObject(objectSha);
    }
    while(std::getline(in_, line));

    out_ << std::endl;
}


struct SHA1Hasher
{
    size_t operator()(const SHA1& oth)
    {
        auto hasher = std::hash<std::string>();
        return hasher(static_cast<const std::string&>(oth));
    }
};


// Fetch objects with it's references
void Fetch::FetchObject(const SHA1& sha)
{
    // Todo: implement multiprocessing logic

    std::queue<SHA1> queue;
    queue.push(sha);

    std::unordered_set<std::string> downloaded;
    std::unordered_set<std::string> queued;

    while (!queue.empty())
    {
       const SHA1& objectSha = queue.front();
       std::cerr << "Fetching object " << std::string(objectSha) << std::endl;

        std::optional<GitHelpers::GitObjectWriter> writer;
        {
            auto decryptor = encryptionStrategy_->MakeDecryptor([&](std::string_view decryptedChunk)
            {
                writer->Write(decryptedChunk);
            });

            std::vector<char> metaValueBuffer;

            remote_->FetchObject(objectSha,
             [&](std::string_view chunk)
             {
                 if (writer)
                 {
                     decryptor(chunk);
                     return;
                 }

                 metaValueBuffer.insert(
                         metaValueBuffer.begin(),
                         chunk.begin(),
                         chunk.end()
                 );

                 if (metaValueBuffer.size() >= OBJECT_META_LEN)
                 {
                     ObjectMetaValue metaValue;

                     std::copy(
                             chunk.begin(),
                             chunk.begin() + OBJECT_META_LEN,
                             metaValue.begin()
                     );

                     ObjectMeta meta{metaValue};

                     writer = GitHelpers::WriteObject(meta.Type());

                     if (metaValueBuffer.size() > OBJECT_META_LEN)
                     {
                         // Send the rest of buffer
                         std::string_view metaValueBufferView = {metaValueBuffer.data(),
                                                                 metaValueBuffer.size()};

                         decryptor({
                              metaValueBuffer.data() + OBJECT_META_LEN,
                              metaValueBuffer.size() - OBJECT_META_LEN
                         });
                     }
                 }

                 metaValueBuffer.clear();
             });
        }

       SHA1 sh = writer->Finalize();

       if((std::string)sh != (std::string)objectSha)
       {
           throw std::runtime_error("Object hash differs");
       }

       queue.pop();

       std::cerr << "fetched " << (std::string)sh << std::endl;
       downloaded.insert(sh);

       const auto references = GitHelpers::ReferencedObjects(sh);
       for(const auto& ref: references)
       {
           if (downloaded.find(ref) != downloaded.end())
           {
               continue;
           }

           if (GitHelpers::ObjectExists(ref))
           {
               std::cerr << "object exists " << (std::string)ref << std::endl;
               downloaded.insert(ref);
               continue;
           }

           std::cerr << "reference added " << (std::string)ref << std::endl;

           if (queued.find(ref) == queued.end())
           {
               queued.insert(ref);
               queue.push(ref);
           }
       }

       std::cerr << "queue size " << queue.size()
                 << "downloaded " << downloaded.size() << std::endl;
    }
}
