#ifndef GIT_REMOTE_ALIEN_FETCH_H
#define GIT_REMOTE_ALIEN_FETCH_H

#include <iostream>
#include <memory>
#include <Encryption/IEncryptionStrategy.h>

#include "IGitCommandHandler.h"
#include "Remote/IRemoteProvider.h"

namespace GitRemote::Handlers
{
    class Fetch: public IGitCommandHandler
    {
    public:
        Fetch(std::istream& in, std::ostream& out,
              Remote::IRemoteProviderSPtr remote,
              Encryption::IEncryptionStrategySPtr encryptionStrategy
        );

    public:
        void HandleGitCommand(std::string_view line) override;

    private:
        // Fetch object with it's references
        void FetchObject(const SHA1& sha);

    private:
        std::istream& in_;
        std::ostream& out_;
        std::shared_ptr<Remote::IRemoteProvider> remote_;
        Encryption::IEncryptionStrategySPtr encryptionStrategy_;
    };
}


#endif //GIT_REMOTE_ALIEN_FETCH_H
