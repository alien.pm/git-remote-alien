#ifndef GIT_REMOTE_ALIEN_IGITCOMMANDHANDLER_H
#define GIT_REMOTE_ALIEN_IGITCOMMANDHANDLER_H

#include <string>

namespace GitRemote::Handlers
{
    class IGitCommandHandler
    {
    public:
        virtual void HandleGitCommand(std::string_view line) = 0;
        virtual ~IGitCommandHandler() = default;
    };
};

#endif //GIT_REMOTE_ALIEN_IGITCOMMANDHANDLER_H
