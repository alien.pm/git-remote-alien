#include "List.h"

using namespace GitRemote;
using namespace GitRemote::Handlers;

List::List(std::istream &in,
           std::ostream &out,
           Remote::RemoteContextSPtr context,
           std::shared_ptr<Remote::IRemoteProvider> remote
)
    : in_(in), out_(out), context_(std::move(context)), remote_(std::move(remote))
{ }

void List::HandleGitCommand(std::string_view line)
{
    const bool listForPush = line.find("for-push") != std::string::npos;

    context_->refs = remote_->GetRefs();
    if (context_->refs.empty())
    {
        if (listForPush)
        {
            context_->isFirstPush = true;
        }
        else
        {
            throw std::runtime_error("Remote repository is empty");
        }
    }

    for(const Ref& r: context_->refs)
    {
        out_ << r << std::endl;
    }

    if (!listForPush)
    {
        const std::optional<std::string> headSymRef =
                remote_->ReadSymbolicRef("HEAD");

        if (headSymRef)
        {
            const std::string& value = *headSymRef;

            out_ << "@" << value << " HEAD" << std::endl;
        }
        else
        {
            //todo: log
            std::cerr << "No default branch on remote" << std::endl;
        }
    }

    out_ << std::endl;
}
