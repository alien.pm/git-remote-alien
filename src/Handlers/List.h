#ifndef GIT_REMOTE_ALIEN_LIST_H
#define GIT_REMOTE_ALIEN_LIST_H

#include "IGitCommandHandler.h"
#include "Remote/RemoteContext.h"
#include "Remote/IRemoteProvider.h"
#include "Encryption/IEncryptionStrategy.h"

namespace GitRemote::Handlers
{
    class List: public IGitCommandHandler
    {
    public:
        List(std::istream& in,
             std::ostream& out,
             Remote::RemoteContextSPtr context,
             Remote::IRemoteProviderSPtr remote
        );

    public:
        void HandleGitCommand(std::string_view line) override;

    private:
        std::istream& in_;
        std::ostream& out_;
        std::shared_ptr<Remote::IRemoteProvider> remote_;
        Remote::RemoteContextSPtr context_;
    };
}

#endif //GIT_REMOTE_ALIEN_LIST_H
