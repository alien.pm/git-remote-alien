#include <sstream>

#include "Push.h"
#include "GitHelpers.h"
#include "ObjectMeta.h"

using namespace GitRemote;
using namespace GitRemote::Handlers;

Push::Push(std::istream &in,
           std::ostream &out,
           Remote::RemoteContextSPtr context,
           Remote::IRemoteProviderSPtr remote,
           Encryption::IEncryptionStrategySPtr encryptionStrategy
)
        : in_(in), out_(out), context_(std::move(context)), remote_(std::move(remote)),
        encryptionStrategy_(std::move(encryptionStrategy))
{ }

void Push::HandleGitCommand(std::string_view commandLine)
{
    std::string line { commandLine };

    std::string remoteHead;

    do
    {
        if (line.empty())
        {
            break;
        }

        std::string pushCommand; // always eq "push"
        std::string srcDstPair; // e.g. "refs/heads/master:refs/heads/master"
                                //         { src }        :     { dst }

        std::istringstream lineStream (line);
        if (!std::getline(lineStream, pushCommand, ' ') ||
            !std::getline(lineStream, srcDstPair))
        {
            throw UnexpectedGitOutput();
        }

        std::istringstream srcDstPairStream(srcDstPair);

        std::string src;
        std::string dst;

        if (!std::getline(srcDstPairStream, src, ':') ||
            !std::getline(srcDstPairStream, dst))
        {
            throw UnexpectedGitOutput();
        }

        if (src.empty())
        {
            DeleteRef(dst);
        }
        else
        {
            PushRef(src, dst);
        }

        if (context_->isFirstPush)
        {
            std::string headSymRef = GitHelpers::SymbolicRefValue("HEAD");
            if (remoteHead.empty() || (src == headSymRef))
            {
                remoteHead = dst;
            }
        }
    }
    while(std::getline(in_, line));

    // TODO: write head

    if (context_->isFirstPush)
    {
        context_->isFirstPush = false;
        remote_->WriteSymbolicRef("HEAD", remoteHead);
    }

    out_ << std::endl;
}

void Push::PushRef(std::string_view inputSrc, std::string_view dst)
{
    if (inputSrc.empty())
    {
        return;
    }

    if (dst.empty())
    {
        return;
    }


    bool forceOverwrite = false;

    std::string src;
    if (inputSrc[0] == '+')
    {
        forceOverwrite = true;
        src = src.substr(1);
    }
    else
    {
        src = inputSrc;
    }

    SHA1Vt presentObjectsExclude = pushed_;

    std::transform(
        context_->refs.cbegin(), context_->refs.cend(),
        std::back_inserter(presentObjectsExclude),
        [](const Ref& ref) -> SHA1
        {
            return ref.sha;
        }
    );

    const SHA1Vt objectsToPush = GitHelpers::RevList(src, presentObjectsExclude);

    for (const SHA1& objectSha: objectsToPush)
    {
        std::cerr
            << "Pushing object "
            << static_cast<std::string_view>(objectSha)
            << std::endl;

        PushObject(objectSha);
    }

    const SHA1 dstRefValue = GitHelpers::RefValue(dst);
    WriteRef(dst, dstRefValue, forceOverwrite);
    pushed_.push_back(GitHelpers::RefValue(src));
}

void Push::WriteRef(std::string_view refPath, const SHA1& refValue, bool forceOverwrite)
{
    if (forceOverwrite)
    {
        remote_->WriteRef(refPath, refValue, Remote::WriteMode::Rewrite);
        return;
    }

    auto remoteRefIt = std::find_if(
            context_->refs.cbegin(),
            context_->refs.cend(),
            [&](const Ref& ref)
            {
                return ref.path == refPath;
            }
    );

    if (remoteRefIt != context_->refs.cend())
    {
        // Remote ref exists
        const SHA1& remoteRefValue = remoteRefIt->sha;
        if (!GitHelpers::ObjectExists(remoteRefValue))
        {
            throw std::runtime_error("Push failed: remote ref is set to object missing in local repo. "
                                     "Please pull changes first");
        }

        if (!GitHelpers::IsAncestorOf(remoteRefValue, refValue))
        {
            throw std::runtime_error("Push failed: non fast forward");
        }

        // Fast forward
        remote_->WriteRef(refPath, refValue, Remote::WriteMode::Rewrite);
    }
    else
    {
        // Remote ref is missing, create one
        remote_->WriteRef(refPath, refValue, Remote::WriteMode::Create);
    }
}

void Push::PushObject(const SHA1& objectSha)
{
    auto objectWriter = remote_->PushObject(objectSha);

    GitObjectType type = GitHelpers::ObjectType(objectSha);
    ObjectMeta meta;
    meta.SetType(GitHelpers::ObjectType(objectSha));
    objectWriter->WriteChunk({ meta.Value().data(), meta.Value().size() });

    auto encryptedWriter = encryptionStrategy_->MakeEncryptor([&](std::string_view objectChunk)
    {
        objectWriter->WriteChunk(objectChunk);
    });

    GitHelpers::ReadObject(objectSha, type, std::move(encryptedWriter));

    objectWriter->Finish();
}

void Push::DeleteRef(std::string_view remoteRef)
{
    // TODO: implement
}
