#ifndef GIT_REMOTE_ALIEN_PUSH_H
#define GIT_REMOTE_ALIEN_PUSH_H

#include <iostream>
#include <memory>

#include "IGitCommandHandler.h"
#include "Remote/RemoteContext.h"
#include "Remote/IRemoteProvider.h"
#include "Encryption/IEncryptionStrategy.h"

namespace GitRemote::Handlers
{
    class Push: public IGitCommandHandler
    {
    public:
        Push(std::istream& in,
             std::ostream& out,
             Remote::RemoteContextSPtr context,
             Remote::IRemoteProviderSPtr remote,
             Encryption::IEncryptionStrategySPtr encryptionStrategy
         );

    public:
        void HandleGitCommand(std::string_view line) override;

    private:
        void PushRef(std::string_view src, std::string_view dst);
        void WriteRef(std::string_view refPath, const SHA1& value, bool forceOverwrite);
        void DeleteRef(std::string_view remoteRef);
        void PushObject(const SHA1& object);

    private:
        std::istream& in_;
        std::ostream& out_;
        Remote::RemoteContextSPtr context_;
        Remote::IRemoteProviderSPtr remote_;
        SHA1Vt pushed_;
        Encryption::IEncryptionStrategySPtr encryptionStrategy_;
    };
}


#endif //GIT_REMOTE_ALIEN_PUSH_H
