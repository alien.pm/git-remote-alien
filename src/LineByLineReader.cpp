#include <cstring>
#include "LineByLineReader.h"

using namespace GitRemote;

LineByLineReader::LineByLineReader(LineReader lineReader) :
        lineReader_(std::move(lineReader))
{
}

void LineByLineReader::operator()(std::string_view chunk)
{
    std::copy(chunk.begin(), chunk.end(), std::back_inserter(buf_));
    HandleLines();
}

void LineByLineReader::HandleLines()
{
    if (buf_.empty())
    {
        return;
    }

    auto begin = buf_.begin();

    // Call lineReader for each line in the buffer
    while (true)
    {
        const auto it = std::find(begin, buf_.end(), '\n');

        if (it == buf_.end())
        {
            break;
        }

        size_t lineLen = std::distance(begin, it);

        // Ignore windows carriage return character
        if (lineLen > 0)
        {
            if (*(it - 1) == '\r')
            {
                --lineLen;
            }
        }

        std::string_view lineView {&*begin, lineLen};
        PostLine(lineView);

        begin = it + 1;
    }

    // No lines read
    if (begin == buf_.begin())
    {
        return;
    }

    // Handle the remainder
    const size_t remainderLen = std::distance(begin, buf_.end());

    if (remainderLen == 0)
    {
        buf_.clear();
    }
    else
    {
        std::memmove(buf_.data(), &*begin, remainderLen);
        buf_.resize(remainderLen);
    }
};

void GitRemote::LineByLineReader::Flush()
{
    if (!buf_.empty())
    {
        PostLine({buf_.data(), buf_.size()});
        buf_.clear();
    }
}

LineByLineReader::~LineByLineReader()
{
    Flush();
}

void LineByLineReader::PostLine(std::string_view line)
{
    if (lineReader_ != nullptr)
    {
        try
        {
            lineReader_(line);
        }
        catch(const DoneReadingException&)
        {
            lineReader_ = nullptr;
        }
    }
}
