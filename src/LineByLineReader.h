#ifndef GIT_REMOTE_ALIEN_LINEBYLINEREADER_H
#define GIT_REMOTE_ALIEN_LINEBYLINEREADER_H

#include <vector>
#include "GitTypes.h"
#include "Readers.h"

namespace GitRemote
{
    /* Handles the data chunks, calls line reader
     * for each line */

    class LineByLineReader: public ChunkReader
    {
    public:
        struct DoneReadingException {};

        explicit LineByLineReader(LineReader lineReader);
        ~LineByLineReader();

        void operator()(std::string_view chunk);

        // Calls Flush when destructed
        void Flush();

    private:
        void HandleLines();
        void PostLine(std::string_view line);
    private:
        std::vector<char> buf_;
        LineReader lineReader_;
    };

}


#endif //GIT_REMOTE_ALIEN_LINEBYLINEREADER_H
