#include "ObjectMeta.h"

namespace GitRemote
{
    namespace
    {
        const size_t OBJECT_TYPE_POS = 0;
    }

    GitRemote::ObjectMeta::ObjectMeta(const GitRemote::ObjectMetaValue& value)
            : value_(value)
    { }

    GitRemote::ObjectMeta::ObjectMeta()
        : value_({})
    { }

    GitRemote::GitObjectType GitRemote::ObjectMeta::Type() const
    {
        char c = value_[OBJECT_TYPE_POS];
        return static_cast<GitObjectType>(c);
    };

    void ObjectMeta::SetType(GitObjectType type)
    {
        value_[OBJECT_TYPE_POS] = static_cast<char>(type);
    }

    const ObjectMetaValue& ObjectMeta::Value() const
    {
        return value_;
    }
}
