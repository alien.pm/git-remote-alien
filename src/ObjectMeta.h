#ifndef GIT_REMOTE_ALIEN_OBJECTMETA_H
#define GIT_REMOTE_ALIEN_OBJECTMETA_H

#include <array>
#include "GitTypes.h"

namespace GitRemote
{
    static const size_t OBJECT_META_LEN = 1;

    using ObjectMetaValue = std::array<char, OBJECT_META_LEN>;

    // Additional information stored on the beginning of an object on remote
    // (currently only it's type)
    class ObjectMeta
    {
    public:
        explicit ObjectMeta(const ObjectMetaValue& value);
        ObjectMeta();

        void SetType(GitObjectType type);

        GitObjectType Type() const;

        const ObjectMetaValue& Value() const;

    private:
        ObjectMetaValue value_;

    };
};

#endif //GIT_REMOTE_ALIEN_OBJECTMETA_H
