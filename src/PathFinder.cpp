#include <filesystem>
#include <sstream>

#include "PathFinder.h"

std::optional<std::filesystem::path> GitRemote::FindPATHFile(std::string_view file)
{
    const char* path = getenv("PATH");

    if (path == nullptr)
    {
        // Todo: log

        return {};
    }

    std::istringstream pathStream(path);

    std::string entry;

#ifndef _WIN32
    const char PATH_DELIMITER = ':';
#else
    const char PATH_DELIMITER = ';';
#endif

    while(std::getline(pathStream, entry, PATH_DELIMITER))
    {
        std::filesystem::path entryPath = entry;
        entryPath /= file;

        if (std::filesystem::exists(entryPath))
        {
            return entryPath;
        }
    }

    return {};
}
