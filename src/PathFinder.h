#ifndef GIT_REMOTE_ALIEN_PATHFINDER_H
#define GIT_REMOTE_ALIEN_PATHFINDER_H

#include <optional>
#include <filesystem>

namespace GitRemote
{
    std::optional<std::filesystem::path> FindPATHFile(std::string_view file);
}

#endif //GIT_REMOTE_ALIEN_PATHFINDER_H
