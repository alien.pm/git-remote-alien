#include "ProcessHelpers.h"

#include <process.hpp>

namespace proc = TinyProcessLib;

namespace
{
    // Todo replace with logger
    void StubOutputHandler(const char* data, size_t size)
    {
    }
}

namespace GitRemote::ProcessHelpers
{
    ErrorExitException::ErrorExitException(int ec)
            : std::runtime_error("Process exited with unexpected code"),
              ec_(ec) { }

    int ErrorExitException::ExitCode() const
    {
        return ec_;
    }

    void ReadProcessOutput(
        const CommandArguments& arguments, 
        ChunkReader outputReader, 
        const ProcessStringType& startupDirectory
    )
    {
        proc::Process process(arguments,
                              startupDirectory,
                              [outputReader(std::move(outputReader))]
                                      (const char* data, size_t size)
                              {
                                  outputReader({data, size});
                              }, StubOutputHandler);

        const int ec = process.get_exit_status();

        if (ec != 0)
        {
            throw ErrorExitException(ec);
        }
    }

    bool ProcessExitOk(const CommandArguments& arguments, const ProcessStringType& startupDirectory)
    {
        proc::Process process(
                arguments,
                startupDirectory,
                StubOutputHandler,
                StubOutputHandler
        );

        const int ec = process.get_exit_status();
        return ec == 0;
    }

    CommandArguments::CommandArguments(std::initializer_list<std::string> args)
    {
        for (const std::string& a : args)
        {
            push_back(ProcessStringType(a.begin(), a.end()));
        }
    }

    void CommandArguments::push_back(const std::wstring& val)
    {
        std::vector<ProcessStringType>& self = *this;
        self.push_back(ProcessStringType(val.begin(), val.end()));
    }

    void CommandArguments::push_back(std::string_view val)
    {
        std::vector<ProcessStringType>& self = *this;
        self.push_back(ProcessStringType(val.begin(), val.end()));
    }
}
