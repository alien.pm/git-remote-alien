#ifndef GIT_REMOTE_ALIEN_PROCESSHELPERS_H
#define GIT_REMOTE_ALIEN_PROCESSHELPERS_H

#include <stdexcept>
#include <process.hpp>
#include "Readers.h"

namespace GitRemote::ProcessHelpers
{
    class ErrorExitException: public std::runtime_error
    {
    public:
        explicit ErrorExitException(int ec);
        int ExitCode() const;
    private:
        int ec_;
    };

    /* Process library use std::string / std::wstring for paths
    * depending on platform */

    using ProcessStringType = TinyProcessLib::Process::string_type;

    class CommandArguments: public std::vector<ProcessStringType>
    {
    public:
        CommandArguments() = default;
        CommandArguments(std::initializer_list<std::string> args);

        void push_back(const std::wstring& val);
        void push_back(std::string_view val);
    };

    void ReadProcessOutput(
            const CommandArguments& arguments,
            ChunkReader outputReader,
            const ProcessStringType& startupDirectory = {}
    );

    bool ProcessExitOk(
            const CommandArguments& arguments,
            const ProcessStringType& startupDirectory = {}
    );
}

#endif //GIT_REMOTE_ALIEN_PROCESSHELPERS_H
