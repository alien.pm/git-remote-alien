#ifndef GIT_REMOTE_ALIEN_READERS_H
#define GIT_REMOTE_ALIEN_READERS_H

#include <functional>
#include <string_view>

namespace GitRemote
{
    using ChunkReader = std::function<void(std::string_view chunk)>;
    using LineReader = std::function<void(std::string_view line)>;
}

#endif //GIT_REMOTE_ALIEN_READERS_H
