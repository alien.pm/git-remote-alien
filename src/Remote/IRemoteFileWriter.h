#ifndef GIT_REMOTE_ALIEN_IREMOTEFILEWRITER_H
#define GIT_REMOTE_ALIEN_IREMOTEFILEWRITER_H

#include <memory>
#include <string_view>

namespace GitRemote::Remote
{
    class IRemoteFileWriter
    {
    public:
        virtual void WriteChunk(std::string_view chunk) = 0;
        virtual void Finish() = 0;

        virtual ~IRemoteFileWriter() = default;
    };

    using IRemoteFileWriterPtr = std::unique_ptr<IRemoteFileWriter>;
};

#endif //GIT_REMOTE_ALIEN_IREMOTEFILEWRITER_H
