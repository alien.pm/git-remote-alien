#ifndef GIT_REMOTE_ALIEN_IREMOTEPROVIDER_H
#define GIT_REMOTE_ALIEN_IREMOTEPROVIDER_H

#include <string>
#include <optional>
#include <memory>

#include "Readers.h"
#include "GitTypes.h"
#include "IRemoteFileWriter.h"

namespace GitRemote::Remote
{
    enum class WriteMode
    {
        Create,
        Rewrite
    };

    class IRemoteProvider
    {
    public:
        virtual bool ConnectivityCheck() = 0;

        virtual std::optional<std::string> ReadSymbolicRef(std::string_view path) = 0;
        virtual void WriteSymbolicRef(std::string_view path, std::string_view refPath) = 0;

        virtual void FetchObject(const SHA1& sha, ChunkReader chunkReader) = 0;
        virtual IRemoteFileWriterPtr PushObject(const SHA1& sha) = 0;

        // May throw EmptyRepositoryException
        virtual RefVt GetRefs() = 0;
        virtual void WriteRef(std::string_view refPath, const SHA1& value, WriteMode mode) = 0;


        virtual ~IRemoteProvider() = default;
    };

    using IRemoteProviderSPtr = std::shared_ptr<IRemoteProvider>;
    using RemoteProviderFactory = std::function<IRemoteProviderSPtr(std::string_view arguments)>;
}

#endif //GIT_REMOTE_ALIEN_IREMOTEPROVIDER_H
