#include <sstream>

#include "BaseFilesystemRemoteProvider.h"

namespace GitRemote::Remote
{
    namespace
    {
        const std::string REFS_DIR = "refs";
        const std::filesystem::path OBJECTS_DIR = "objects";

    }

    std::filesystem::path BaseFilesystemRemoteProvider::ObjectPath
    (
        const std::filesystem::path& base,
        const SHA1& sha
    )
    {
        const std::string &shaString = sha;
        return base / OBJECTS_DIR / shaString.substr(0, 2) / shaString.substr(2);
    }

    std::optional<std::string> BaseFilesystemRemoteProvider::ReadSymbolicRef(
            std::string_view path
    )
    {
        std::string symRefFileLine;

        try
        {
            std::stringstream symRefContentStream = ReadOutFileContent(path);
            std::getline(symRefContentStream, symRefFileLine);
        }
        catch (FileNotFoundException& e)
        {
            return {};
        }

        return ParseRefFile(symRefFileLine);
    }

    void BaseFilesystemRemoteProvider::WriteSymbolicRef(
            std::string_view path,
            std::string_view ref
    )
    {
        auto writer = WriteFile(path);

        std::ostringstream contentStream;
        contentStream << "ref: " << ref << "\n";

        const std::string contentString = contentStream.str();
        writer->WriteChunk(contentString);
        writer->Finish();
    };

    RefVt BaseFilesystemRemoteProvider::GetRefs()
    {
        FileVt refFiles;
        RefVt refs;
        refs.reserve(refFiles.size());

        try
        {
            TraverseDirectoryRecursive(REFS_DIR, refFiles);
        }
        catch (FileNotFoundException&)
        {
            return refs;
        }


        std::transform(refFiles.cbegin(), refFiles.cend(), std::back_inserter(refs),
            [&](const std::filesystem::path& refPath) -> Ref
            {
                std::stringstream refStream = ReadOutFileContent(refPath);
                std::string ref = refStream.str();
                auto refValue = SHA1::FromString(ref);
                return {refPath.generic_string(), std::move(refValue)};
            }
        );

        return refs;
    }

    void BaseFilesystemRemoteProvider::WriteRef(std::string_view refPath, const SHA1 &value, WriteMode /* mode */)
    {
        auto writer = WriteFile(refPath);

        writer->WriteChunk(value);
        writer->Finish();
    }

    IRemoteFileWriterPtr BaseFilesystemRemoteProvider::PushObject(const SHA1 &sha)
    {
        return WriteFile(ObjectPath({}, sha));
    }

    void BaseFilesystemRemoteProvider::FetchObject(const SHA1 &sha, ChunkReader chunkReader)
    {
        const std::filesystem::path objectPath = ObjectPath({}, sha);
        ReadFile(objectPath, std::move(chunkReader));
    }

    void BaseFilesystemRemoteProvider::TraverseDirectoryRecursive(
            const std::filesystem::path& dirPath, FileVt& result
    )
    {
        auto content = ReadDirectory(dirPath);

        for (const FilesystemEntry& entry: content)
        {
            auto entryPath = dirPath / entry.name;

            switch (entry.type)
            {
                case FilesystemEntryType::File:
                    result.push_back(entryPath);
                    break;

                case FilesystemEntryType::Directory:
                    TraverseDirectoryRecursive(entryPath, result);
                    break;
            }
        }
    }

    std::stringstream BaseFilesystemRemoteProvider::ReadOutFileContent(const std::filesystem::path &path)
    {
        std::stringstream contentStream;
        ReadFile(path, [&](std::string_view chunk)
        {
            contentStream.write(chunk.data(), chunk.size());
        });

        return contentStream;
    }
}
