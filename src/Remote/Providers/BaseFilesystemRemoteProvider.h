#ifndef GIT_REMOTE_ALIEN_BASEFILESYSTEMREMOTEPROVIDER_H
#define GIT_REMOTE_ALIEN_BASEFILESYSTEMREMOTEPROVIDER_H

#include <filesystem>

#include "Remote/IRemoteProvider.h"

namespace GitRemote::Remote
{
    enum FilesystemEntryType
    {
        File,
        Directory
    };

    struct FilesystemEntry
    {
        FilesystemEntryType type;
        std::string name;
    };

    using FileVt = std::vector<std::filesystem::path>;
    using DirectoryContent = std::vector<FilesystemEntry>;

    class FileNotFoundException: public std::runtime_error
    {
    public:
        explicit FileNotFoundException(): std::runtime_error("Remote file not found") {}
    };

    class BaseFilesystemRemoteProvider: public IRemoteProvider
    {
    public:
        // Provides implementation for IRemoteProvider methods
        std::optional<std::string> ReadSymbolicRef(std::string_view name) override;
        void WriteSymbolicRef(std::string_view path, std::string_view ref) override;
        void FetchObject(const SHA1& sha, ChunkReader chunkReader) override;
        IRemoteFileWriterPtr PushObject(const SHA1& sha) override ;
        RefVt GetRefs() override;
        void WriteRef(std::string_view refPath, const SHA1& value, WriteMode mode) override;

    protected:
        // Throws FileNotFoundException
        virtual void ReadFile(std::filesystem::path path, ChunkReader reader) = 0;

        // Creates the intermediate directories automatically
        virtual IRemoteFileWriterPtr WriteFile(std::filesystem::path path) = 0;

        // Throws FileNotFoundException
        virtual DirectoryContent ReadDirectory(std::filesystem::path) = 0;

    public:
        static std::filesystem::path ObjectPath(
            const std::filesystem::path& base,
            const SHA1& sha
        );

    private:
        using DirectoryVisitor = std::function<
                void(const std::filesystem::path& currDirPath, std::vector<std::filesystem::path> files)
        >;

        std::stringstream ReadOutFileContent(const std::filesystem::path& path);
        void TraverseDirectoryRecursive(const std::filesystem::path& directory, FileVt& result);
    };
};

#endif //GIT_REMOTE_ALIEN_BASEFILESYSTEMREMOTEPROVIDER_H
