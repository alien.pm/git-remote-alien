#include <fstream>

#include "LocalFilesystemFileWriter.h"

namespace GitRemote::Remote
{
    namespace
    {
        class LocalFilesystemFileWriter: public IRemoteFileWriter
        {
        public:
            explicit LocalFilesystemFileWriter(const std::filesystem::path& path)
                : file_(path.generic_string(), std::fstream::out | std::fstream::binary) { }

        public:
            void WriteChunk(std::string_view chunk) override
            {
                file_.write(chunk.data(), chunk.size());

                if (file_.fail())
                {
                    throw std::runtime_error("Error writing file chunk");
                }
            }

            void Finish() override
            {
                file_.close();
            }

            ~LocalFilesystemFileWriter() override
            {
                Finish();
            }

        private:
            std::ofstream file_;
        };
    };

    IRemoteFileWriterPtr MakeLocalFilesystemFileWriter(const std::filesystem::path& path)
    {
        return std::make_unique<LocalFilesystemFileWriter>(path);
    }
}
