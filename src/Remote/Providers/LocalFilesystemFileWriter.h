#ifndef GIT_REMOTE_ALIEN_LOCALFILESYSTEMFILEWRITER_H
#define GIT_REMOTE_ALIEN_LOCALFILESYSTEMFILEWRITER_H

#include <filesystem>

#include "Remote/IRemoteFileWriter.h"

namespace GitRemote::Remote
{
    IRemoteFileWriterPtr MakeLocalFilesystemFileWriter(const std::filesystem::path& path);
}

#endif //GIT_REMOTE_ALIEN_LOCALFILESYSTEMFILEWRITER_H
