#include <cstring>
#include <fstream>

#include "LocalFilesystemRemoteProvider.h"
#include "LocalFilesystemFileWriter.h"
#include "BaseFilesystemRemoteProvider.h"

using namespace GitRemote;
using namespace GitRemote::Remote;

namespace fs = std::filesystem;

namespace
{
    size_t CHUNK_SIZE = 16 * 1024 * 1024;
}

class FSRemoteProvider: public BaseFilesystemRemoteProvider
{
public:
    explicit FSRemoteProvider(std::filesystem::path repoRoot)
        : repoRoot_(std::move(repoRoot)) { }

    bool ConnectivityCheck() override
    {
        return true;
    }

protected:
    void ReadFile(std::filesystem::path path, ChunkReader chunkReader) override
    {
        const auto absolutePath = repoRoot_ / path;

        std::ifstream fileStream{absolutePath, std::ios::binary};

        if (!fileStream)
        {
            std::cerr << "Failed to open the object file "
                      << std::strerror(errno) << std::endl;

            throw std::runtime_error("Failed to open the object file");
        }

        static std::vector<char> chunkBuffer(CHUNK_SIZE);

        while (!fileStream.eof())
        {
            fileStream.read(chunkBuffer.data(), chunkBuffer.size());
            const std::size_t bytesRead = fileStream.gcount();

            if (bytesRead > 0)
            {
                chunkReader({chunkBuffer.data(), bytesRead});
            }
        }
    }

    IRemoteFileWriterPtr WriteFile(std::filesystem::path path) override
    {
        const auto absolutePath = repoRoot_ / path;
        const auto dirPath = absolutePath.parent_path();

        std::filesystem::create_directories(dirPath);

        return MakeLocalFilesystemFileWriter(absolutePath);
    }

    DirectoryContent ReadDirectory(std::filesystem::path path) override
    {
        const auto absolutePath = repoRoot_ / path;

        DirectoryContent content;
        
        if (!std::filesystem::exists(absolutePath))
        {
            throw FileNotFoundException();
        }

        for (const fs::directory_entry& entry:
                fs::directory_iterator(absolutePath))
        {
            if(entry.is_regular_file())
            {
                content.push_back(
                {
                   FilesystemEntryType::File,
                   entry.path().filename().generic_string()
                });

                continue;
            }

            if (entry.is_directory())
            {
                content.push_back({
                    FilesystemEntryType::Directory,
                    entry.path().filename().generic_string()
                });

                continue;
            }
        }

        return content;
    }

private:
    std::filesystem::path repoRoot_;
};

namespace GitRemote::Remote
{
    RemoteProviderFactory MakeFSRemoteProviderFactory()
    {
        return [](std::string_view arguments) -> IRemoteProviderSPtr
        {
            return  std::make_shared<FSRemoteProvider>(std::filesystem::path(arguments));
        };
    }
}

