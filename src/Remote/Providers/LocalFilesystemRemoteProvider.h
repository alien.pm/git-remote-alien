#ifndef GIT_REMOTE_ALIEN_LOCALFILESYSTEMREMOTEPROVIDER_H
#define GIT_REMOTE_ALIEN_LOCALFILESYSTEMREMOTEPROVIDER_H

#include "Remote/IRemoteProvider.h"

namespace GitRemote::Remote
{
    RemoteProviderFactory MakeFSRemoteProviderFactory();
};


#endif //GIT_REMOTE_ALIEN_LOCALFILESYSTEMREMOTEPROVIDER_H
