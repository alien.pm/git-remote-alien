#include <sstream>
#include <optional>
#include <ProcessHelpers.h>

#include "RcloneHelpers.h"
#include "RcloneFileWriter.h"
#include "process.hpp"

namespace proc = TinyProcessLib;

namespace GitRemote::Remote
{
    namespace
    {
        struct Context
        {
            std::unique_ptr<proc::Process> process;
            std::stringstream stdoutAccumulator;
            std::stringstream stderrAccumulator;
        };
    }

    class RcloneFileWriter: public IRemoteFileWriter
    {
    public:
        explicit RcloneFileWriter(std::string rclonePath)
         : rclonePath_(std::move(rclonePath)) { }

        void WriteChunk(std::string_view chunk) override
        {
            EnsureProcessRunning();
            context_->process->write(chunk.data(), chunk.size());
        }

        void Finish() override
        {
            EnsureProcessRunning();

            context_->process->close_stdin();

            const int ec = context_->process->get_exit_status();

            const std::string outputHash = context_->stdoutAccumulator.str();
            const std::string stdErr = context_->stderrAccumulator.str();

            if (ec != 0)
            {
                throw ProcessHelpers::ErrorExitException(ec);
            }
        }

    private:
        void EnsureProcessRunning()
        {
            if (context_)
            {
                return;
            }

            context_ = std::make_shared<Context>();
           context_->process = std::make_unique<proc::Process>(
                    RcloneStartupParams({
                       "rcat",
                        rclonePath_
                    }),
                    ProcessHelpers::ProcessStringType(),
                    [context(context_)](const char* data, size_t size) // Stdout handler
                    {
                        context->stdoutAccumulator.write(data, size);
                    },
                    [context(context_)](const char* data, size_t size){
                        context->stderrAccumulator.write(data, size);
                    },
                    true // Open stdin
            );
        }

    private:
        std::string rclonePath_;
        std::shared_ptr<Context> context_;
    };

    IRemoteFileWriterPtr MakeRcloneFileWriter(std::string rclonePath)
    {
        return std::make_unique<RcloneFileWriter>(std::move(rclonePath));
    }
}
