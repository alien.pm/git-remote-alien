#ifndef GIT_REMOTE_ALIEN_RCLONEFILEWRITER_H
#define GIT_REMOTE_ALIEN_RCLONEFILEWRITER_H

#include "Remote/IRemoteFileWriter.h"
#include <string>

namespace GitRemote::Remote
{
    IRemoteFileWriterPtr MakeRcloneFileWriter(std::string rclonePath /* e.g. configName:fileName.txt */ );
};

#endif //GIT_REMOTE_ALIEN_RCLONEFILEWRITER_H
