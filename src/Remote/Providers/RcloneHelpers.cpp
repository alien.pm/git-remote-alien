#include <filesystem>

#include "RcloneHelpers.h"
#include "ProcessHelpers.h"
#include "PathFinder.h"

namespace GitRemote::Remote
{
    ProcessHelpers::CommandArguments RcloneStartupParams(
            const ProcessHelpers::CommandArguments& arguments
    )
    {
        static std::optional<std::filesystem::path> rclonePath =
                []()
                {
                    auto rclonePath = FindPATHFile("rclone");
                    if (!rclonePath)
                    {
                        rclonePath = FindPATHFile("rclone.exe");
                    }

                    return rclonePath;
                }();

        if (!rclonePath)
        {
            throw std::runtime_error("rclone binary not found in system PATH");
        }

        ProcessHelpers::CommandArguments params;
        params.reserve(arguments.size() + 1);

        params.push_back(rclonePath->generic_string());
        params.insert(params.end(), arguments.begin(), arguments.end());

        return params;
    }
}
