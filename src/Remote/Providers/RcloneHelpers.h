#ifndef GIT_REMOTE_ALIEN_RCLONEHELPERS_H
#define GIT_REMOTE_ALIEN_RCLONEHELPERS_H

#include <ProcessHelpers.h>

namespace GitRemote::Remote
{
    ProcessHelpers::CommandArguments RcloneStartupParams(
        const ProcessHelpers::CommandArguments& arguments
    );
}

#endif //GIT_REMOTE_ALIEN_RCLONEHELPERS_H
