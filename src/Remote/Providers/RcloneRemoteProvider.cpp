#include <fstream>
#include <sstream>

#include "LineByLineReader.h"
#include "RcloneRemoteProvider.h"
#include "BaseFilesystemRemoteProvider.h"
#include "RcloneHelpers.h"
#include "RcloneFileWriter.h"

using namespace GitRemote;
using namespace GitRemote::Remote;
using namespace GitRemote::ProcessHelpers;

namespace fs = std::filesystem;

class RcloneRemoteProvider: public BaseFilesystemRemoteProvider
{
public:
    explicit RcloneRemoteProvider(std::string configName, std::filesystem::path relativePath)
            : configName_(std::move(configName)), relativePath_(std::move(relativePath)) { }

    bool ConnectivityCheck() override
    {
        std::string rclonePath = configName_ + ":/";

        return ProcessHelpers::ProcessExitOk(
            RcloneStartupParams({"lsf", rclonePath})
        );
    }

protected:
    void ReadFile(std::filesystem::path path, ChunkReader chunkReader) override
    {
        const auto absolutePath = relativePath_ / path;
        std::string rclonePath = configName_ + ":" + absolutePath.generic_string();

        ProcessHelpers::ReadProcessOutput(
            RcloneStartupParams({"cat", rclonePath}), std::move(chunkReader)
        );
    }

    IRemoteFileWriterPtr WriteFile(std::filesystem::path path) override
    {
        const auto absolutePath = relativePath_ / path;
        std::string rclonePath = configName_ + ":" + absolutePath.generic_string();

        return MakeRcloneFileWriter(rclonePath);
    }

    DirectoryContent ReadDirectory(std::filesystem::path path) override
    {
        DirectoryContent content;

        try
        {
            ProcessHelpers::ReadProcessOutput(RcloneStartupParams({"lsf", RcloneEntryPath(path)}),
              LineByLineReader([&](std::string_view line)
               {
                   if (line.empty())
                   {
                      return;
                   }

                   const char lastChar = line[line.size() - 1];

                   const bool isDirectory = lastChar == '/';

                   if (isDirectory)
                   {
                       content.push_back(
                               {
                                       FilesystemEntryType::Directory,
                                       std::string(line.substr(0, line.size() - 1))
                               });
                   }
                   else
                   {
                       content.push_back(
                               {
                                       FilesystemEntryType::File,
                                       std::string(line)
                               });
                   }
               }));

        }
        catch (const ErrorExitException& e)
        {
            // TODO: handle not existing directory correctly
            return content;
        }

        return content;
    }

    std::string RcloneEntryPath(const std::filesystem::path& path)
    {
        const auto absolutePath = relativePath_ / path;
        std::string rclonePath = configName_ + ":" + absolutePath.generic_string();

        return rclonePath;
    }

private:
    std::string configName_;
    std::filesystem::path relativePath_;

};

namespace GitRemote::Remote
{
    RemoteProviderFactory MakeRcloneRemoteProviderFactory()
    {
        // e.g. "configName:relative/path"
        return [](std::string_view arguments) -> IRemoteProviderSPtr
        {
            std::istringstream argumentStream((std::string(arguments)));
            std::string configName;
            std::string relativePath;
            std::getline(argumentStream, configName, ':');
            std::getline(argumentStream, relativePath);

            return  std::make_shared<RcloneRemoteProvider>(std::move(configName), std::move(relativePath));
        };
    }
}
