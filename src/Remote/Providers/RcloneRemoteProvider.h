#ifndef GIT_REMOTE_ALIEN_RCLONEREMOTEPROVIDER_H
#define GIT_REMOTE_ALIEN_RCLONEREMOTEPROVIDER_H

#include "Remote/IRemoteProvider.h"

namespace GitRemote::Remote
{
    RemoteProviderFactory MakeRcloneRemoteProviderFactory();
};


#endif //GIT_REMOTE_ALIEN_RCLONEREMOTEPROVIDER_H
