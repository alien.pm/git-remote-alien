#ifndef GIT_REMOTE_ALIEN_REMOTECONTEXT_H
#define GIT_REMOTE_ALIEN_REMOTECONTEXT_H

#include <memory>

#include "GitTypes.h"

namespace GitRemote::Remote
{
    struct RemoteContext
    {
        RefVt refs;
        bool isFirstPush = false;
    };

    using RemoteContextSPtr = std::shared_ptr<RemoteContext>;
};

#endif //GIT_REMOTE_ALIEN_REMOTECONTEXT_H
