#include <unordered_map>

#include "RemoteProviderSelector.h"

#include "Remote/Providers/LocalFilesystemRemoteProvider.h"
#include "Remote/Providers/RcloneRemoteProvider.h"

namespace GitRemote::Remote
{
    namespace
    {
        const std::unordered_map<std::string /* id */, RemoteProviderFactory> REMOTE_PROVIDERS
            {
                    {"fs", MakeFSRemoteProviderFactory()},
                    {"rclone", MakeRcloneRemoteProviderFactory()}
            };
    };

    RemoteProviderSelector MakeRemoteProviderSelector()
    {
        // e.g. remoteUrl "alien://#encryptionParams^remoteProvider:arguments"
        return [](std::string_view remoteUrl) -> IRemoteProviderSPtr
        {
            size_t providerIdIdx = remoteUrl.find('@');
            ++providerIdIdx;

            if (providerIdIdx == remoteUrl.npos)
            {
                throw std::runtime_error("Unable to find remote provider signature ^");
            }

            size_t remoteProviderArgumentsPos = remoteUrl.find(':', providerIdIdx);
            const size_t providerIdLen = remoteProviderArgumentsPos - providerIdIdx;

            std::string_view providerId = remoteUrl.substr(providerIdIdx, providerIdLen);

            ++remoteProviderArgumentsPos;
            const size_t providerArgsLen = remoteUrl.size() - remoteProviderArgumentsPos;

            std::string_view providerArguments = remoteUrl.substr(remoteProviderArgumentsPos);

            auto remoteProviderFactory = REMOTE_PROVIDERS.find(std::string(providerId));

            if (remoteProviderFactory == REMOTE_PROVIDERS.end())
            {
                throw std::runtime_error(
                        "Unable to find remote provider having id " +
                        std::string(providerId)
                );
            }

            return remoteProviderFactory->second(providerArguments);
        };
    }
}
