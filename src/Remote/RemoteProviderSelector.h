#ifndef GIT_REMOTE_ALIEN_REMOTEPROVIDERSELECTOR_H
#define GIT_REMOTE_ALIEN_REMOTEPROVIDERSELECTOR_H

#include <functional>
#include "IRemoteProvider.h"

namespace GitRemote::Remote
{
    using RemoteProviderSelector = std::function<
            IRemoteProviderSPtr(std::string_view /* url e.g. alien://#encryptionParams^fsProvider:args */)
    >;

    RemoteProviderSelector MakeRemoteProviderSelector();
};

#endif //GIT_REMOTE_ALIEN_REMOTEPROVIDERSELECTOR_H
