#include <iostream>
#include <memory>
#include <thread>

#include "GitInteractor.h"
#include "GitHelpers.h"
#include "Handlers/Fetch.h"
#include "Handlers/List.h"
#include "Handlers/Push.h"
#include "Remote/RemoteProviderSelector.h"
#include "Encryption/EncryptionStrategySelector.h"

#include <plog/Log.h>

using namespace GitRemote;
using namespace GitRemote::GitHelpers;

size_t count = 0;

int main(int argc, char** argv)
{
    plog::init(plog::Severity::verbose, "/Users/excrypt/alien.log");

    bool stop = true;

    if (argc != 3)
    {
        std::cerr << "git-remote-alien is called with unexpected argument count "
                  << argc << std::endl;

        return 1;
    }

    while(!stop)
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    const std::string remoteName = argv[1];
    const std::string url = argv[2];

    auto providerSelector = Remote::MakeRemoteProviderSelector();
    Remote::IRemoteProviderSPtr remoteProvider = providerSelector(url);
    Encryption::IEncryptionStrategySPtr encryptionStrategy = Encryption::SelectEncryptionStrategy(url);

    if (!remoteProvider->ConnectivityCheck())
    {
        std::cerr << "Connection check failed" << std::endl;

        return 1;
    }

    auto remoteContext = std::make_shared<Remote::RemoteContext>();

    CommandHandlerMap handlers;

    handlers["list"] = std::make_unique<Handlers::List>(
        std::cin,
        std::cout,
        remoteContext,
        remoteProvider
    );

    handlers["fetch"] = std::make_unique<Handlers::Fetch>(
        std::cin,
        std::cout,
        remoteProvider,
        encryptionStrategy
    );

    handlers["push"] = std::make_unique<Handlers::Push>(
        std::cin,
        std::cout,
        remoteContext,
        remoteProvider,
        encryptionStrategy
    );

    GitInteractor helper {std::cin, std::cout, std::move(handlers) };
    helper.CommunicationLoop();

    return 0;
}