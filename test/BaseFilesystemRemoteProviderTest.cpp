#include <gtest/gtest.h>

#include "Remote/Providers/BaseFilesystemRemoteProvider.h"

using namespace GitRemote;

TEST(GitHelpers, ObjectPath)
{
    const std::filesystem::path basePath = "/repo";
    const SHA1 object = SHA1::FromString("c368dd896ae7bf31df62d6d3a3e4d6a452199f5f");
    const std::filesystem::path expectedPath = "/repo/objects/c3/68dd896ae7bf31df62d6d3a3e4d6a452199f5f";

    EXPECT_EQ(expectedPath, GitRemote::Remote::BaseFilesystemRemoteProvider::ObjectPath(basePath, object));
};
