cmake_minimum_required(VERSION 3.0.0)

project(alien-test)

set(CMAKE_CXX_STANDARD 17)

enable_testing()

include_directories(../src)

set(SOURCE_FILES
        ../src/GitHelpers.cpp
        ../src/GitHelpers.h
        ../src/GitObjectWriter.cpp
        ../src/GitObjectWriter.h
        ../src/GitTypes.cpp
        ../src/GitTypes.h
        ../src/LineByLineReader.cpp
        ../src/LineByLineReader.h
        ../src/PathFinder.cpp
        ../src/PathFinder.h
        ../src/GitInteractor.cpp
        ../src/GitInteractor.h
        ../src/Handlers/Fetch.cpp
        ../src/Handlers/Fetch.h
        ../src/Remote/IRemoteProvider.h
        ../src/Remote/Providers/LocalFilesystemRemoteProvider.cpp
        ../src/Remote/Providers/LocalFilesystemRemoteProvider.h
        ../src/ObjectMeta.h
        ../src/ObjectMeta.cpp
        ../src/ProcessHelpers.cpp
        ../src/ProcessHelpers.h
        ../src/Encryption/StubEncryptionStrategy.cpp
        ../src/Encryption/AesCbc256KeyEncryptionStrategy.cpp
        ../src/Remote/Providers/BaseFilesystemRemoteProvider.cpp
        ../src/Remote/Providers/BaseFilesystemRemoteProvider.h
        ../src/Remote/Providers/LocalFilesystemFileWriter.cpp
        ../src/Remote/Providers/LocalFilesystemFileWriter.h

        LineByLineReaderTest.cpp
        GitTypesTest.cpp
        GitHelpersTest.cpp
        BaseFilesystemRemoteProviderTest.cpp
        EncryptorsTest.cpp
        )

add_executable(alien-test main.cpp ${SOURCE_FILES})

# Run tests if not in debug mode
if (NOT CMAKE_BUILD_TYPE STREQUAL "Debug")

ADD_CUSTOM_COMMAND(TARGET alien-test
POST_BUILD
COMMAND alien-test
WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/test/testdata")

endif (NOT CMAKE_BUILD_TYPE STREQUAL "Debug")

include_directories(../third-party/tiny-process-library)
target_link_libraries(alien-test
        PRIVATE
        gtest
        gmock
        tiny-process-library
)

add_test(AllTestsInMain alien-test)