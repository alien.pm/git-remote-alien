#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "Encryption/IEncryptionStrategy.h"
#include "Encryption/StubEncryptionStrategy.h"
#include "Encryption/AesCbc256KeyEncryptionStrategy.h"

using namespace GitRemote::Encryption;

class EncryptorsTestFixture: public ::testing::TestWithParam<IEncryptionStrategySPtr>
{ };

/* TEST LIST
 *
 * SingleByteInput
 * BinaryInput
 * ChunkedInput "hello world" => "he" "llo" "world"
 * RandomInputSeries
 * MultipleOfTwoSizeInput (0, 1, 2, 3) {16) (0, 2, 4, 6) {16) (0, 4, 8, 12) {16}
 * 1 2 4 8 16 32 64 128 256 512 1024
 * MultipleOfTwoSizeInputChunked (0, 1, 2, 3) {16) (0, 2, 4, 6) {16) (0, 4, 8, 12) {16}
 * 1 2 4 8 16 32 64 128 256 512 1024
 * Stress_VariosInputSize_VariousChunkSize
 */

TEST_P(EncryptorsTestFixture, EmptyInput)
{
    const auto strategy = GetParam();

    std::stringstream decryptedStream;

    {
        auto decryptor = strategy->MakeDecryptor(
            [&](std::string_view input)
            {
                decryptedStream.write(input.data(), input.size());
            });

        auto encryptor = strategy->MakeEncryptor(std::move(decryptor));

        encryptor("");
    }

    const std::string decrypted = decryptedStream.str();

    EXPECT_EQ("", decrypted);
}

TEST_P(EncryptorsTestFixture, SingleByteInput)
{
    const auto strategy = GetParam();

    std::stringstream decryptedStream;

    {
        auto decryptor = strategy->MakeDecryptor([
            &](std::string_view input)
            {
                decryptedStream.write(input.data(), input.size());
            });

        auto encryptor = strategy->MakeEncryptor(std::move(decryptor));

        encryptor("a");
    }

    const std::string decrypted = decryptedStream.str();

    EXPECT_EQ("a", decrypted);
}

INSTANTIATE_TEST_CASE_P(
        StubEncryptorTests,
        EncryptorsTestFixture,
        ::testing::Values(MakeStubEnryptionStrategy(""))
);

INSTANTIATE_TEST_CASE_P(
        Aes256EncryptionStrategy,
        EncryptorsTestFixture,
        ::testing::Values(MakeAes256KeyEnryptionStrategy(""))
);