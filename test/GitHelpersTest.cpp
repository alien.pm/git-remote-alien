#include "GitHelpers.h"
#include "GitObjectWriter.h"
#include "GitTypes.h"
#include "ProcessHelpers.h"

#include <gtest/gtest.h>
#include <fstream>
#include <random>

using namespace GitRemote;
using namespace GitRemote::ProcessHelpers;

namespace
{
    const SHA1 TESTDATA_FIRST_COMMIT        = SHA1::FromString("c368dd896ae7bf31df62d6d3a3e4d6a452199f5f");
    const SHA1 TESTDATA_FIRST_COMMIT_TREE   = SHA1::FromString("eaf719bdd60fd555562a1a45e2788791463fd91a");
    const SHA1 TESTDATA_HELLOWORLD_BLOB     = SHA1::FromString("3b18e512dba79e4c8300dd08aeb37f8e728b8dad");
    const SHA1 TESTDATA_SECOND_COMMIT       = SHA1::FromString("fd21811a16d0f5963a52755674a16d0a347f1dfb");
    const SHA1 TESTDATA_SECOND_COMMIT_TREE  = SHA1::FromString("c172bc468738553f1cd3da0b5dd252e18c003ec8");
    const SHA1 TESTDATA_HISTORY_HELLOWORLD_BLOB    = SHA1::FromString("ce013625030ba8dba906f756967f9e9ca394464a");
    const std::string TESTDATA_2ND_COMMIT_TAG      = "2nd_commit";

    const SHA1 RANDOM_SHA1                 = SHA1::FromString("6216f8a75fd5bb3d5f22b6f9958cdede3fc086c3");

    const SHA1Vt TESTDATA_FIRST_COMMIT_REVS
         {
             TESTDATA_FIRST_COMMIT,
             TESTDATA_FIRST_COMMIT_TREE,
             TESTDATA_HELLOWORLD_BLOB
         };

    const SHA1Vt TESTDATA_SECOND_COMMIT_EXCLUDE_FIRST_COMMIT_REVS
            {
                    TESTDATA_SECOND_COMMIT,
                    TESTDATA_SECOND_COMMIT_TREE,
                    TESTDATA_HISTORY_HELLOWORLD_BLOB
            };

    const std::string TESTDATA_HELLOWORLD_CONTENT = "hello world\n";
    const std::string TESTDATA_FIRST_COMMIT_SHORT = "c368dd";
}

TEST(GitHelpers, GitCommandOutput_GitVersion)
{
    std::string result;
    EXPECT_NO_THROW(result = GitHelpers::CommandString({"--version"}));
    EXPECT_TRUE(result.find("version") != std::string::npos);
}

TEST(GitHelpers, GitCommandOutput_InvalidCommandThrowException)
{
    EXPECT_THROW(GitHelpers::CommandString({"wrongcommand"}),
                 ErrorExitException);
}

TEST(GitHelpers, GitCommandOk_GitStatus)
{
    EXPECT_TRUE(GitHelpers::CommandOk({"--version"}));
}

TEST(GitHelpers, GitCommandOk_WrongCommand)
{
    EXPECT_FALSE(GitHelpers::CommandOk({"wrongcommand"}));
}

TEST(GitHelpers, GitObjectExists_TestDataFirstCommit)
{
    EXPECT_TRUE(GitHelpers::ObjectExists(TESTDATA_FIRST_COMMIT));
}

TEST(GitHelpers, GitObjectExists_RandomHash)
{
    EXPECT_FALSE(GitHelpers::ObjectExists(RANDOM_SHA1));
}

TEST(GitHelpers, GitHistoryExists_TestDataFirstCommit)
{
    EXPECT_TRUE(GitHelpers::HistoryExists(TESTDATA_FIRST_COMMIT));
}

TEST(GitHelpers, GitHistoryExists_RandomHash)
{
    EXPECT_FALSE(GitHelpers::HistoryExists(RANDOM_SHA1));
}

// Todo: cover negative HistoryExists cases

TEST(GitHelpers, GitRefValue_FirstCommitShort)
{
    EXPECT_EQ(
            GitHelpers::RefValue(std::string{TESTDATA_FIRST_COMMIT_SHORT}),
            TESTDATA_FIRST_COMMIT
    );
}

TEST(GitHelpers, GitRefValue_RandomHash_ReturnAsIs)
{
    EXPECT_EQ(GitHelpers::RefValue(std::string{RANDOM_SHA1}), RANDOM_SHA1);
}

TEST(GitHelpers, GitSymbolicRefValue_SetAndGet)
{
    GitHelpers::SetSymbolicRef("TEST", "123");
    const std::string result = GitHelpers::SymbolicRefValue("TEST");

    EXPECT_EQ(result, "123");
}

TEST(GitHelpers, GitSymbolicRefValue_RandomName)
{
    EXPECT_THROW(GitHelpers::SymbolicRefValue("senseless"), std::exception);
}

TEST(GitHelpers, ObjectType_Commit)
{
    EXPECT_EQ(GitHelpers::ObjectType(TESTDATA_FIRST_COMMIT), GitObjectType::Commit);
}

TEST(GitHelpers, ObjectType_Blob)
{
    EXPECT_EQ(GitHelpers::ObjectType(TESTDATA_HELLOWORLD_BLOB), GitObjectType::Blob);
}

TEST(GitHelpers, ObjectType_Tree)
{
    EXPECT_EQ(GitHelpers::ObjectType(TESTDATA_FIRST_COMMIT_TREE), GitObjectType::Tree);
}

TEST(GitHelpers, ObjectType_Tag)
{
    EXPECT_EQ(GitHelpers::ObjectType(TESTDATA_2ND_COMMIT_TAG), GitObjectType::Tag);
}

TEST(GitHelpers, ReadObject_Tree)
{
    std::ostringstream outStream;

    GitHelpers::ReadObject(TESTDATA_FIRST_COMMIT_TREE, GitObjectType::Tree,
                           [&outStream](std::string_view chunk)
            {
                outStream.write(chunk.data(), chunk.size());
            });

    std::string result = outStream.str();
    const std::string expectedBeginning = "100644 helloworld.txt";
    EXPECT_TRUE(result.find(expectedBeginning) != std::string::npos);
    EXPECT_EQ(result.size(), 42);
}

TEST(GitHelpers, ReadObject_InvalidType_CommitAsBlob)
{
    EXPECT_THROW(
        GitHelpers::ReadObject(TESTDATA_FIRST_COMMIT, GitObjectType::Blob,
                               [](std::string_view) {}),
        std::exception
    );
}

TEST(GitHelpers, WriteObject_ObjectHash_Equals_HelloWorldBlob)
{
    auto writer = GitHelpers::WriteObject(GitObjectType::Blob);

    writer.Write({
        TESTDATA_HELLOWORLD_CONTENT.data(),
        TESTDATA_HELLOWORLD_CONTENT.size()
    });

    const SHA1 helloWorldHash = writer.Finalize();

    EXPECT_EQ(helloWorldHash, TESTDATA_HELLOWORLD_BLOB);
}

TEST(GitHelpers, WriteObject_ObjectHash_LargeObject)
{
    auto writer = GitHelpers::WriteObject(GitObjectType::Blob);

    for (size_t i = 0; i < 1000; ++i)
    {
        writer.Write({"123456789\n"});
    }


    const SHA1 objectHash = writer.Finalize();
    const SHA1 er = SHA1::FromString("0b0acc85640f62f4b78a68af8850a02f5316800e");

    EXPECT_EQ(objectHash, er);
}

TEST(GitHelpers, WriteRandomObject_ReadAndCheck)
{
    // Random object is used to ensure that object actually writes on each test run.
    std::string randomObject =
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    auto writer = GitHelpers::WriteObject(GitObjectType::Blob);

    std::random_device rd;
    std::mt19937 generator(rd());
    std::shuffle(randomObject.begin(), randomObject.end(), generator);

    writer.Write(randomObject);
    const SHA1 objectHash = writer.Finalize();

    std::ostringstream outStream;

    GitHelpers::ReadObject(objectHash, GitObjectType::Blob,
            [&outStream](std::string_view chunk)
            {
                outStream.write(chunk.data(), chunk.size());
            });

    const std::string result = outStream.str();

    EXPECT_EQ(randomObject, result);
}

TEST(GitHelpers, RevList_FirstCommit)
{
    SHA1Vt result = GitHelpers::RevList(TESTDATA_FIRST_COMMIT);

    EXPECT_EQ(TESTDATA_FIRST_COMMIT_REVS, result);
};

TEST(GitHelpers, RevList_SecondCommitExcludeFirstCommit)
{
    SHA1Vt result = GitHelpers::RevList(TESTDATA_SECOND_COMMIT, {TESTDATA_FIRST_COMMIT});

    EXPECT_EQ(TESTDATA_SECOND_COMMIT_EXCLUDE_FIRST_COMMIT_REVS, result);
};

TEST(GitHelpers, RevList_SecondCommitExcludeFirstCommit_IgnoreWrongExclude)
{
    SHA1Vt result = GitHelpers::RevList(TESTDATA_SECOND_COMMIT,
            {TESTDATA_FIRST_COMMIT, RANDOM_SHA1});

    EXPECT_EQ(TESTDATA_SECOND_COMMIT_EXCLUDE_FIRST_COMMIT_REVS, result);
};

TEST(GitHelpers, ReferencedObjects_BLOB_ReturnEmptyVector)
{
    SHA1Vt result = GitHelpers::ReferencedObjects(TESTDATA_HELLOWORLD_BLOB);
    EXPECT_TRUE(result.empty());
}

TEST(GitHelpers, ReferencedObjects_2ndCommitTag_ReturnSecondCommit)
{
    SHA1Vt result = GitHelpers::ReferencedObjects(TESTDATA_2ND_COMMIT_TAG);
    SHA1Vt er = { TESTDATA_SECOND_COMMIT};

    EXPECT_EQ(result, er);
}

TEST(GitHelpers, ReferencedObjects_FirstCommitTree)
{
    SHA1Vt result = GitHelpers::ReferencedObjects(TESTDATA_FIRST_COMMIT_TREE);
    SHA1Vt er = { TESTDATA_HELLOWORLD_BLOB };

    EXPECT_EQ(result, er);
}

TEST(GitHelpers, ReferencedObjects_SecondCommitTree)
{
    SHA1Vt result = GitHelpers::ReferencedObjects(TESTDATA_SECOND_COMMIT_TREE);
    SHA1Vt er = { TESTDATA_HELLOWORLD_BLOB, TESTDATA_HISTORY_HELLOWORLD_BLOB };

    EXPECT_EQ(result, er);
}

TEST(GitHelpers, ReferencedObjects_FirstCommitWithoutParent)
{
    SHA1Vt result = GitHelpers::ReferencedObjects(TESTDATA_FIRST_COMMIT);

    SHA1Vt er = { TESTDATA_FIRST_COMMIT_TREE };

    EXPECT_EQ(result, er);
}

TEST(GitHelpers, ReferencedObjects_SecondCommitWithParentFirst)
{
    SHA1Vt result = GitHelpers::ReferencedObjects(TESTDATA_SECOND_COMMIT);

    SHA1Vt er = {
            TESTDATA_SECOND_COMMIT_TREE, // It's tree
            TESTDATA_FIRST_COMMIT // parent
    };

    EXPECT_EQ(result, er);
}

TEST(GitHelpers, RemoteUrl_WrongName)
{
    EXPECT_THROW(GitHelpers::RemoteUrl("missingremote"), std::exception);
}

TEST(GitHelpers, RemoteUrl_OriginContainsTestDataSubstring)
{
    const std::string url = GitHelpers::RemoteUrl("origin");
    EXPECT_TRUE(url.find("testdata") != std::string::npos);
}

TEST(GitHelpers, Ref_OstreamWriting)
{
    Ref ref{"testname", TESTDATA_FIRST_COMMIT};

    std::ostringstream stream;
    stream << ref;

    const std::string result = stream.str();
    const std::string er = "c368dd896ae7bf31df62d6d3a3e4d6a452199f5f testname";

    EXPECT_EQ(result, er);
};
