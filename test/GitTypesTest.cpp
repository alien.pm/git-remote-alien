#include "GitTypes.h"
#include <gtest/gtest.h>

using namespace GitRemote;

TEST(GitTypes, ParseObjectType_Commit)
{
    EXPECT_EQ(GitObjectType::Commit, ParseObjectType("commit"));
}

TEST(GitTypes, ParseObjectType_Blob)
{
    EXPECT_EQ(GitObjectType::Blob, ParseObjectType("blob"));
}

TEST(GitTypes, ParseObjectType_Tree)
{
    EXPECT_EQ(GitObjectType::Tree, ParseObjectType("tree"));
}

TEST(GitTypes, ParseObjectType_Tag)
{
    EXPECT_EQ(GitObjectType::Tag, ParseObjectType("tag"));
}

TEST(GitTypes, ParseObjectType_WrongType_Throw)
{
    EXPECT_THROW(ParseObjectType("senseless"), WrongObjectTypeException);
}

namespace
{
    const std::string RANDOM_SHA_STRING = "6216f8a75fd5bb3d5f22b6f9958cdede3fc086c3";
}

TEST(GitTypes, SHA1FromString_RegualarHash)
{
    std::string asString = SHA1::FromString(RANDOM_SHA_STRING);
    EXPECT_EQ(RANDOM_SHA_STRING, asString);
}

TEST(GitTypes, SHA1FromString_FixExtraNewline)
{
    std::string asString = SHA1::FromString(RANDOM_SHA_STRING + '\n');
    EXPECT_EQ(RANDOM_SHA_STRING, asString);
}

TEST(GitTypes, SHA1FromString_WrongLengthThrowException)
{
    EXPECT_THROW(SHA1::FromString(RANDOM_SHA_STRING + "garbage"), WrongSHA1StringException);
}

TEST(GitTypes, ParseRefFile_RegularRef)
{
    const std::string inputRef = "ref: refs/heads/master";
    const std::string& er = "refs/heads/master";

    EXPECT_EQ(er, ParseRefFile(inputRef));
}


TEST(GitTypes, ParseRefFile_NoPrefixThrowException)
{
    const std::string inputRef = "refs/heads/master";

    EXPECT_THROW(ParseRefFile(inputRef), std::exception);
}
