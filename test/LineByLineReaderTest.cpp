#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "LineByLineReader.h"

using namespace GitRemote;

using MockLineReader = testing::MockFunction<void(std::string_view)>;

TEST(LineByLineReader, NoLineDelimiersFlushCalled)
{
    MockLineReader lineReader;
    std::string_view testString = "test";

    EXPECT_CALL(lineReader, Call(testString)).Times(1);

    LineByLineReader reader(lineReader.AsStdFunction());
    reader(testString);
    reader.Flush();
}

TEST(LineByLineReader, UnixLineDelimiter)
{
    MockLineReader lineReader;
    std::string_view testString = "test\n";
    std::string_view er = "test";

    EXPECT_CALL(lineReader, Call(er)).Times(1);

    LineByLineReader reader(lineReader.AsStdFunction());
    reader(testString);
    reader.Flush();
}

TEST(LineByLineReader, ChunkedInputUnixLineDelimiter)
{
    MockLineReader lineReader;
    std::string_view er = "test";

    EXPECT_CALL(lineReader, Call(er)).Times(1);

    LineByLineReader reader(lineReader.AsStdFunction());
    reader({"te"});
    reader({"s"});
    reader({"t"});
    reader({"\n"});
    reader.Flush();
}

TEST(LineByLineReader, ChunkedInputUnixLineDelimiterExtraDataFlushedOnDestruction)
{
    MockLineReader lineReader;
    std::string_view er = "test";

    EXPECT_CALL(lineReader, Call(er)).Times(1);
    EXPECT_CALL(lineReader, Call(std::string_view{"remainder"}))
        .Times(1);

    LineByLineReader reader(lineReader.AsStdFunction());

    reader({"te"});
    reader({"s"});
    reader({"t"});
    reader({"\n"});

    reader({"remainder"});
}

TEST(LineByLineReader, ChunkedWithMultilineInput)
{
    MockLineReader lineReader;

    EXPECT_CALL(lineReader, Call({"test1"})).Times(1);
    EXPECT_CALL(lineReader, Call({"test2"})).Times(1);
    EXPECT_CALL(lineReader, Call({"test3"})).Times(1);
    EXPECT_CALL(lineReader, Call({"test4"})).Times(1);

    LineByLineReader reader(lineReader.AsStdFunction());
    reader({"te"});
    reader({"s"});
    reader({"t"});
    reader({"1\n"});
    reader({"test2\ntest3\n"});
    reader({"test4"});
    reader.Flush();
}

TEST(LineByLineReader, EmptyLines)
{
    MockLineReader lineReader;

    EXPECT_CALL(lineReader, Call({"test1"})).Times(1);
    EXPECT_CALL(lineReader, Call({""})).Times(1);
    EXPECT_CALL(lineReader, Call({"test3"})).Times(1);

    LineByLineReader reader(lineReader.AsStdFunction());
    reader({"te"});
    reader({"s"});
    reader({"t"});
    reader({"1\n"});
    reader({"\ntest3\n"});
    reader.Flush();
}

TEST(LineByLineReader, ChunkedWithMultilineInputWindowsDelimiter)
{
    MockLineReader lineReader;

    EXPECT_CALL(lineReader, Call({"test1"})).Times(1);
    EXPECT_CALL(lineReader, Call({"test2"})).Times(1);
    EXPECT_CALL(lineReader, Call({"test3"})).Times(1);
    EXPECT_CALL(lineReader, Call({"test4"})).Times(1);

    LineByLineReader reader(lineReader.AsStdFunction());
    reader({"te"});
    reader({"s"});
    reader({"t"});
    reader({"1\r\n"});
    reader({"test2\r\ntest3\r\n"});
    reader({"test4"});
    reader.Flush();
}

TEST(LineByLineReader, DoneReadingThrown_CallbackStopCalling)
{
    MockLineReader lineReader;

    EXPECT_CALL(lineReader, Call({"test1"})).Times(1);
    EXPECT_CALL(lineReader, Call({"test2"})).Times(1)
        .WillOnce(testing::Throw(LineByLineReader::DoneReadingException{}));

    LineByLineReader reader(lineReader.AsStdFunction());
    reader({"te"});
    reader({"s"});
    reader({"t"});
    reader({"1\r\n"});
    reader({"test2\r\ntest3\r\n"});
    reader({"test4"});
    reader.Flush();
}

